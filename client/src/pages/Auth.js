import { useContext, useState } from "react";
import { Container, Form, Button, NavLink, Col } from "react-bootstrap";
import Card from 'react-bootstrap/Card';
import { useLocation, useNavigate } from "react-router-dom";
import { login, registration } from "../http/userAPI";
import { LOGIN_ROUTE, NOTES_ROUTE, REGISTRATION_ROUTE } from '../utils/constants';
import { Context } from "..";
import { observer } from "mobx-react-lite";

const Auth = observer(() => {
    const { user } = useContext(Context);
    const location = useLocation();
    const isLogin = location.pathname === LOGIN_ROUTE;
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const click = async () => {
        let data;
        if (isLogin) {
            data = await login(username, password);
            user.setIsAuth(true);
            navigate(NOTES_ROUTE);
        } else {
            data = await registration(username, password);
        }
        user.setUser(user);
    }
    return (
        <Container className="d-flex justify-content-center align-items-center"
            style={{ height: window.innerHeight - 54 }}>
            <Card style={{ width: 700 }} className="p-5">
                <h2 className="m-auto">{isLogin ? 'Authorization' : 'Registration'}</h2>
                <Form className="d-flex flex-column">
                    <Form.Control
                        placeholder="Enter userName"
                        className="mt-2"
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        required
                    />
                    <Form.Control
                        placeholder="Enter your password"
                        className="mt-2"
                        type="password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                    <Col className="d-flex justify-content-between mt-3 pl-3 pr-3">
                        {isLogin ?
                            <div>Don't have an account?
                                <NavLink style={{ color: "green", textDecoration: "underline" }}
                                    to={REGISTRATION_ROUTE}
                                    onClick={() => { navigate(REGISTRATION_ROUTE); }}>
                                    Register
                                </NavLink>
                            </div> :
                            <div>
                                Already have an account?
                                <NavLink style={{ color: "green", textDecoration: "underline" }}
                                    to={LOGIN_ROUTE}
                                    onClick={() => { navigate(LOGIN_ROUTE); }}>
                                    Log In
                                </NavLink>
                            </div>
                        }

                        {isLogin ?
                            <Button variant="outline-success" className="mb-3" onClick={click}>
                                Log in
                            </Button> :
                            <Button variant="outline-success" className="mb-3" onClick={click}>
                                Register
                            </Button>
                        }

                    </Col>

                </Form>
            </Card>

        </Container>
    );
});
export default Auth;