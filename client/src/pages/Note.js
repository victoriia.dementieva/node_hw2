import { Button, Col, Container, Image, Row } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import img from '../assets/photo.png';


function Note(params) {
    return (
        <Container>
            <Row style={{
                border: '1px solid #fff',
                borderRadius: '50px',
                background: "linear-gradient(to left,#a7c4af, #fff)"
            }}
                className={'d-flex justify-content-between'}>
                <div className={'col-3'}>
                    <Image width={300} height={300}

                        src={img} />
                </div>

                <Col className={'text-center col-8 p-4 '}>
                    <h2>{`Note № ID`}</h2>
                    <Form.Control
                        value={`text`}
                        disabled />
                </Col>
                <Col>
                    <Button>Edit</Button>
                    <Button>Delete</Button>
                </Col>
            </Row>
        </Container>
    );
};
export default Note;