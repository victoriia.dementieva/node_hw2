import { Button, Container } from 'react-bootstrap';
import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

function User(params) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <Container className='d-flex flex-column'>

            <Button className={'mb-2'}
                variant='outline-success' onClick={handleShow}>
                Change password
            </Button>
            <Button className={'mb-2'}
                variant='outline-dark'>
                Delete profile
            </Button>
            <div>
                <h2>
                    {'user id'}
                </h2>
                <div>
                    {'username'}
                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Label>Old passsword</Form.Label>
                    <Form.Control />
                    <Form.Label>New passsword</Form.Label>
                    <Form.Control />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container >
    );
};
export default User;