export const MAIN_ROUTE = '/';
export const LOGIN_ROUTE = '/login';
export const REGISTRATION_ROUTE = '/register';
export const NOTES_ROUTE = '/notes';
export const USER_ROUTE = '/me';