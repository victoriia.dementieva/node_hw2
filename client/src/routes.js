// import User from "./pages/User";
import NotesList from "./pages/NotesList";
import Note from "./pages/Note";
import Auth from "./pages/Auth";
import Main from "./pages/Main";
import { MAIN_ROUTE, LOGIN_ROUTE, NOTES_ROUTE, REGISTRATION_ROUTE, USER_ROUTE } from "./utils/constants";
import User from "./pages/User";

export const authRoutes = [
    {
        path: NOTES_ROUTE,
        Component: <NotesList />
    },
    {
        path: NOTES_ROUTE + '/:id',
        Component: <Note />
    },
    {
        path: USER_ROUTE,
        Component: <User />
    },
];

export const publicRoutes = [
    {
        path: MAIN_ROUTE,
        Component: <Main />
    },
    {
        path: LOGIN_ROUTE,
        Component: <Auth />
    },
    {
        path: REGISTRATION_ROUTE,
        Component: <Auth />
    }
];