import { Card, Col, Image } from "react-bootstrap";
import img from '../assets/photo.png';
import { NOTES_ROUTE } from "../utils/constants";
import { useNavigate } from "react-router-dom";

const NoteItem = ({ notes }) => {
    const navigate = useNavigate();
    return (
        <Col
            md={2}
            onClick={() => navigate(`${NOTES_ROUTE}/${notes.id}`)}>
            <Card
                style={{
                    width: 150,
                    cursor: 'pointer'
                }}
                className='text-center 
                d-flex justify-content-center 
                align-items-center p-4 mb-4'>
                <Image
                    width={100}
                    height={100}
                    src={img}
                    className='mb-2' />
                <div>
                    {notes.text}
                </div>
            </Card>
        </Col>
    );
};
export default NoteItem;
