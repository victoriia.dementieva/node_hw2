import { observer } from "mobx-react-lite";
import { useContext } from 'react';
import { Row } from "react-bootstrap";
import { Context } from "..";
import NoteItem from "./NoteItem";

const NotesBar = observer(() => {
    const { notes } = useContext(Context);
    return (
        <Row  className="d-flex col-10 mx-auto">
            {notes.notes.map((note) => {
                return <NoteItem
                    key={note.id}
                    notes={note}
                />
            })
            }
        </Row>
    );
});
export default NotesBar;

// the list of notes is empty