import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import React, { useContext } from 'react';
import { Context } from '..';
import { LOGIN_ROUTE, MAIN_ROUTE, USER_ROUTE } from '../utils/constants';
import { Button } from 'react-bootstrap';
import { observer } from 'mobx-react-lite';
import { useNavigate } from "react-router-dom";

const NavBar = observer(() => {
    const { user } = useContext(Context);
    const navigate = useNavigate();
    return (
        <Navbar bg="light" variant="light">
            <Container>
                <Navbar.Brand href={MAIN_ROUTE}>Your Notes</Navbar.Brand>
                {
                    user.isAuth ?
                        <Nav className="ml-auto">
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }}
                                onClick={() => navigate(USER_ROUTE)}>Profile</Button>
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }}
                                className="ml-5"
                                onClick={() => {
                                    user.setIsAuth(false)
                                    navigate(LOGIN_ROUTE);;
                                }}>Log out</Button>
                        </Nav> :
                        <Nav className="ml-auto">
                            <Button style={{
                                background: '#a7c4af',
                                border: 'none'
                            }} onClick={() => {
                                user.setIsAuth(true);
                                navigate(LOGIN_ROUTE);
                            }}>Log in</Button>
                        </Nav>}
            </Container>
        </Navbar >
    );
});

export default NavBar;



