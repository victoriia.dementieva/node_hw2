import { makeAutoObservable } from 'mobx';

export default class NotesStore {
    constructor() {
        this._notes = [
            { id: 1, text: "fff" },
            { id: 2, text: "DDD" },
            { id: 3, text: "fff" },
            { id: 4, text: "DDD" }, 
            { id: 5, text: "fff" },
            { id: 6, text: "DDD" }
        ];
        makeAutoObservable(this);
    }

    setNotes(note) {
        this.notes = note;
    }

    get notes() {
        return this._notes;
    }
}