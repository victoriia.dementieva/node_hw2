import { $host, $authHost } from './index';
import jwt_decode from 'jwt-decode';

export const registration = async (username, password) => {
    const { data } = await $host.post('api/auth/register', { username, password })
    return data;
}
export const login = async (username, password) => {
    const { data } = await $host.post('api/auth/login', { username, password })
    console.log(data);
    return jwt_decode(data.jwt_token);
}
export const check = async () => {
    const response = await $host.post('api/auth/register')
    return response;
}