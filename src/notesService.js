const { Note } = require('./models/Notes.js');
const { User } = require('./models/Users.js');

const getMyNotes = (req, res, next) => {
  return Note.find({
    userId: req.user.userId
  }, '-__v')
    .then((result) => {
      res.status(200).json({ "notes": result });
    });
};

function createNote(req, res, next) {
  const { text } = req.body;
  const note = new Note({
    text,
    userId: req.user.userId
  });

  !req.user.userId ? res.status(400).send({
    "message": "User's ID is required."
  }) :
    note.save().then(() => {
      res.status(200).json(
        {
          "message": "Success"
        });
    });
}

const getNote = (req, res, next) =>
  Note.findById(req.params.id)
    .then((note) => {
      note ?
        res.status(200).json({ "note": note }) :
        res.status(400).json({
          "message": "No notes with the following ID."
        });
    });

const updateMyNoteById = (req, res, next) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).send({
      "message": "text is required",
    });
  }

  return Note.findByIdAndUpdate({
    _id: req.params.id, userId: req.user.userId
  }, {
    $set: { text }
  })
    .then((result) => {
      !result ? res.status(400).send({
        "message": "No notes with the following ID."
      }) :
        res.status(200).send({
          "message": 'Note was updated'
        });
    })

}

const markMyNoteCompletedById = (req, res, next) => {
  return Note.findByIdAndUpdate({
    _id: req.params.id, userId: req.user.userId
  }, {
    $set: { completed: true }
  })
    .then((result) => {
      !result ? res.status(400).send({
        "message": "No notes with the following ID."
      }) :
        res.status(200).json({
          "message": 'Note was marked completed'
        });
    });
}

const deleteNote = (req, res, next) => Note.findByIdAndDelete(req.params.id)
  .then((note) => {
    !note ? res.status(400).send({
      "message": "No notes with the following ID."
    }) :
      res.status(200).json({
        "message": "Success"
      });
  });

module.exports = {
  createNote,
  getNote,
  deleteNote,
  getMyNotes,
  updateMyNoteById,
  markMyNoteCompletedById
};

