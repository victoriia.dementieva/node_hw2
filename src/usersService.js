const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const { Note } = require('./models/Notes');

function getProfile(req, res, next) {
    res.status(200).json(
        {
            "user": {
                "_id": req.user.userId,
                "username": req.user.username,
                "createdDate": new Date()
            }
        });
}

function deleteProfile(req, res, next) {
    const note = Note.deleteMany({
        userId: req.user.userId,
    });

    if (note) {
        return User.findByIdAndDelete(req.user.userId)
            .then((user) => {
                user ?
                    res.status(200).status(200).send({
                        "message": 'Success'
                    }) :
                    res.status(400).send({
                        "message": "Can't find a user",
                    });
            });
    }
    return res.status(400).send({
        "message": 'Something went wrong'
    })
}

async function changePassword(req, res, next) {
    const user = await User.findById(req.user.userId);
    const { oldPassword, newPassword } = req.body;
    console.log(user);
    if (!oldPassword || !newPassword) {
        return res.status(400).send({
            "message": 'enter the password',
        });
    }
    if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
        user.password = await bcrypt.hash(newPassword, 10);
        user.save().then(() => res.status(200).send({
            "message": "Success"
        })).catch(err => {
            next(err);
        });
    } else {
        res.status(400).send({
            "message": "the old password is wrong",
        });
    }

}

module.exports = {
    getProfile,
    deleteProfile,
    changePassword
}