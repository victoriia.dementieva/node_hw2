const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const PORT = process.env.PORT || 8080;
require('dotenv').config();
const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://viktoriiadmntvva:ED7XR1XAlKFCbURM@cluster0.ottiemf.mongodb.net/notes?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { authRouter } = require('./authRouter.js');
const cors = require('cors');

app.use(cors());
app.use(express.json());

let accessLog = fs.createWriteStream(path.join(__dirname, 'requestInfo.log'),
  {
    flags: 'a'
  });

app.use(morgan('tiny', {
  stream: accessLog
}));

app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

app.listen(PORT, () => { console.log(`port: ${PORT}`); });

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
