const express = require('express');
const router = express.Router();
const { getProfile, deleteProfile, changePassword } = require('./usersService.js');
const { authMiddleware } = require('./middleware/authMiddleware');


router.get('/me', authMiddleware, getProfile);

router.delete('/me', authMiddleware, deleteProfile);

router.patch('/me', authMiddleware, changePassword);

module.exports = {
    usersRouter: router,
};
